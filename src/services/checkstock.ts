import http from './http'

function addCheckStock(checkStock: any) {
  const emp = JSON.stringify(checkStock)
  return http.post('/checkstock', checkStock)
}

function updateCheckStock(checkStockId: number, checkStock: any) {
  return http.patch(`/checkstock/${checkStockId}`, checkStock)
}

function delCheckStock(checkStock: any) {
  return http.delete(`/checkstock/${checkStock.checkStockId}`)
}

function getCheckStock(id: number) {
  return http.get(`/checkstock/${id}`)
}

function getCheckStocks() {
  return http.get('/checkstock')
}

export default { addCheckStock, updateCheckStock, delCheckStock, getCheckStock, getCheckStocks }
