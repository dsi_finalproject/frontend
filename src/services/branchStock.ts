import http from '@/services/http'
import branchStock from '@/services/branchStock'
import type { BranchStock } from '@/types/BranchStock'

function getBranchStocks() {
  return http.get('/branchstock')
}

function addBranchStock(branchStock: any) {
  const bs = JSON.stringify(branchStock)
  return http.post('/branchstock', branchStock)
}

function getBranchStock(id: number) {
  return http.get(`/branchstock/${id}`)
}

function updateBranchStock( branchStock: any) {
  return http.patch(`/branchstock/${branchStock.branchStockId}`, branchStock)
}

function deleteBranchStock(branchStock: BranchStock) {
  return http.delete(`/branchstock/${branchStock.branchStockId}`)
}
export default {
  getBranchStocks,
  addBranchStock,
  deleteBranchStock,
  updateBranchStock,
  getBranchStock
}
