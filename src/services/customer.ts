import type { Customer } from '@/types/Customer'
import http from './http'

function addCustomer(customer: Customer) {
  return http.post('/customer', customer)
}

function updateCustomer(customer: Customer) {
  return http.patch(`/customer/${customer.customerId}`, customer)
}

function delCustomer(customer: Customer) {
  return http.delete(`/customer/${customer.customerId}`)
}

function getCustomer(id: number) {
  return http.get(`/customer/${id}`)
}

function getCustomers() {
  return http.get('/customer')
}

function getCustomersTotalBuy() {
  return http.get('/customer/sp')
}

export default {
  addCustomer,
  updateCustomer,
  delCustomer,
  getCustomer,
  getCustomers,
  getCustomersTotalBuy
}
