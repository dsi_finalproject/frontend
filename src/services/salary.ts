import type { Salary } from '@/types/Salary'
import http from './http'

function addsalary(salary: Salary) {
  console.log(JSON.stringify(salary))
  return http.post('/salary', salary)
}

function updatesalary(salary: Salary) {
  return http.patch(`/salary/${salary.salaryId}`, salary)
}

function delsalary(salary: Salary) {
  return http.delete(`/salary/${salary.salaryId}`)
}

function getsalary(id: number) {
  return http.get(`/salary/${id}`)
}

function getsalaries() {
  return http.get('/salary')
}

export default { addsalary, updatesalary, delsalary, getsalary, getsalaries }
