
import type { Invoice } from '@/types/Invoice'
import http from './http'

function addInvoice(invoice: Invoice) {
  return http.post('/invoice', invoice)
}

function updateInvoice(invoiceId: number, invoice: Invoice) {
  return http.patch(`/invoice/${invoiceId}`, invoice)
}

function delInvoice(invoice: any) {
  return http.delete(`/invoice/${invoice.invoiceId}`)
}

function getInvoice(id: number) {
  return http.get(`/invoice/${id}`)
}

function getInvoices() {
  return http.get('/invoice')
}

function delInvoiceDetail(invoiceDetail: any) {
  return http.delete(`/invoice/dt/${invoiceDetail.invoiceDetailid}`)
}

export default {
  addInvoice,
  updateInvoice,
  delInvoice,
  getInvoice,
  getInvoices,
  delInvoiceDetail
}
