import type { Promotion } from '@/types/Promotion'
import http from './http'

function addPromotion(promotion: Promotion) {
  return http.post('/promotion', promotion)
}

function updatePromotion(promotion: Promotion) {
  return http.patch(`/promotion/${promotion.promotionId}`, promotion)
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotion/${promotion.promotionId}`)
}

function getPromotion(id: number) {
  return http.get(`/promotion/${id}`)
}

function getPromotions() {
  return http.get('/promotion')
}

function updatePromotionStatus(promotionId: number, status: number) {
  return http.patch(`/promotion/${promotionId}/status`, { status })
}

export default {
  addPromotion,
  updatePromotion,
  delPromotion,
  getPromotion,
  getPromotions,
  updatePromotionStatus
}
