import type { Receipt } from '@/types/Receipt'
import http from './http'

function addReceipt(Receipt: Receipt) {
  return http.post('/receipt', Receipt)
}

function updateReceipt(Receipt: Receipt) {
  return http.patch(`/receipt/${Receipt.receiptId}`, Receipt)
}

function delReceipt(Receipt: Receipt) {
  return http.delete(`/receipt/${Receipt.receiptId}`)
}

function getReceipt(id: number) {
  return http.get(`/receipt/${id}`)
}

function getReceipts() {
  return http.get('/receipt')
}

export default { addReceipt, updateReceipt, delReceipt, getReceipt, getReceipts }
