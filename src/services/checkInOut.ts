import type { CheckInOut } from '@/types/CheckInOut'
import http from './http'

function checkIn(checkInOut: CheckInOut) {
  return http.post('/checkinout', checkInOut)
}

function checkOut(checkInOut: CheckInOut) {
  return http.patch(`/checkinout/${checkInOut.checkInOutId}`, checkInOut)
}

function del(checkInOut: CheckInOut) {
  return http.delete(`/checkinout/${checkInOut.checkInOutId}`)
}

function getOne(id: number) {
  return http.get(`/checkinout/${id}`)
}

function getAll() {
  return http.get('/checkinout')
}

export default { checkIn, checkOut, del, getOne, getAll }
