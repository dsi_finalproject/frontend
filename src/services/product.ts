import type { Product } from '@/types/Product'
import http from './http'

function addProduct(product: Product) {
  return http.post('/product', product)
}

function updateProduct(product: Product) {
  return http.patch(`/product/${product.productId}`, product)
}

function delProduct(product: Product) {
  return http.delete(`/product/${product.productId}`)
}

function getProduct(id: number) {
  return http.get(`/product/${id}`)
}

function getProducts() {
  return http.get('/product')
}

function addProducts(name: string, price: number, qty: number, category: number) {
  // console.log(`/sp?name=${name}&price=${price}&category=${category}`)
  return http.post(`/product/sp/?name=${name}&price=${price}&qty=${qty}&category=${category}`)
}

function descProducts() {
  // console.log(`/sp?name=${name}&price=${price}&category=${category}`)
  return http.get(`/product/sp/desc`)
}

export default {
  addProduct,
  updateProduct,
  delProduct,
  getProduct,
  getProducts,
  addProducts,
  descProducts
}
