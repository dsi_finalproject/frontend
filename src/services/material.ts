import type { Material } from '@/types/Material'
import http from './http'

function addMaterial(material: Material) {
  return http.post('/material', material)
}

function updateMaterial(material: Material) {
  return http.patch(`/material/${material.materialId}`, material)
}

function delMaterial(material: Material) {
  return http.delete(`/material/${material.materialId}`)
}

function getMaterial(id: number) {
  return http.get(`/material/${id}`)
}

function getMaterials() {
  return http.get('/material')
}

export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials }
