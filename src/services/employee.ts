import type { Employee } from '@/types/Employee'
import http from './http'

function addEmployee(employee: Employee) {
  return http.post('/employee', employee)
}

function updateEmployee(employeeId: number, employee: Employee) {
  return http.patch(`/employee/${employeeId}`, employee)
}

function delEmployee(employee: any) {
  return http.delete(`/employee/${employee.employeeId}`)
}

function findOneSp(id: string) {
  return http.get(`/employee/sp/${id}`)
}

function findOneSpDate(id: string, date: string) {
  return http.get(`/employee/sp/${id}/date/${date}`)
}

function getEmployee(id: number) {
  return http.get(`/employee/${id}`)
}

function getEmployees() {
  return http.get('/employee')
}

export default {
  addEmployee,
  updateEmployee,
  delEmployee,
  getEmployee,
  getEmployees,
  findOneSp,
  findOneSpDate
}
