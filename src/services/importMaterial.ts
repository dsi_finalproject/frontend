import http from './http'

function addImportMaterial(importMaterial: any) {
  const imp = JSON.stringify(importMaterial)
  return http.post('/import', importMaterial)
}

function updateImportMaterial(importMaterialId: number, importMaterial: any) {
  return http.patch(`/import/${importMaterialId}`, importMaterial)
}

function delImportMaterial(importMaterial: any) {
  return http.delete(`/import/${importMaterial.importMaterialId}`)
}

function delImportMaterialDetail(importMaterialDetail: any) {
  return http.delete(`/import/dt/${importMaterialDetail.importMaterialDetailId}`)
}

function getImportMaterial(id: number) {
  return http.get(`/import/${id}`)
}

function getImportMaterials() {
  return http.get('/import')
}

export default {
  addImportMaterial,
  updateImportMaterial,
  delImportMaterial,
  getImportMaterial,
  getImportMaterials,
  delImportMaterialDetail
}
