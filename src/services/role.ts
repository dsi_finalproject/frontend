import type { Role } from '@/types/Role'
import http from './http'

function addrole(role: Role) {
  return http.post('/roles', role)
}

function updaterole(role: Role) {
  return http.patch(`/roles/${role.roleId}`, role)
}

function delrole(role: Role) {
  return http.delete(`/roles/${role.roleId}`)
}

function getrole(id: number) {
  return http.get(`/roles/${id}`)
}

function getroles() {
  return http.get('/roles')
}

export default { addrole, updaterole, delrole, getrole, getroles }
