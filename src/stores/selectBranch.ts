import { ref, computed, onMounted } from 'vue'
import { defineStore } from 'pinia'
import type { Branch } from '@/types/Branch'
import { useBranchStore } from './branch'

export const useSelectBranchStore = defineStore('selectbranch', () => {
  const dialogOpen = ref(false)
  const branchStore = useBranchStore()
  const selectedBranch = ref<Branch>()

  const branchName = ref<string>('บางแสน')

  // const branches = [
  //   { id: 1, name: 'ทั้งหมด' },
  //   { id: 2, name: 'บางแสน' },
  //   { id: 3, name: 'อ่างศิลา' },
  //   { id: 4, name: 'ศรีราชา' }
  // ]

  const branches = ref<Branch[]>([])

  const selectBranch = (branch: { id: number; name: string }) => {
    dialogOpen.value = false
    oldselectedBranch.value = branch.name
  }

  const oldselectedBranch = ref('บางแสน')

  async function findBranch() {
    selectedBranch.value = branchStore.branches.find((branch) => branch.name === branchName.value)
  }

  return { selectBranch, branches, dialogOpen, selectedBranch, branchName, findBranch }
})
