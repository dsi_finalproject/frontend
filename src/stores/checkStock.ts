import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { CheckStock } from '@/types/CheckStock'
import type { CheckStockDetail } from '@/types/CheckStockDetail'
import { useMaterialStore } from './material'
import { useEmployeeStore } from './employee'
import { useBranchStore } from './branch'
import checkStockService from '@/services/checkstock'
import branchStock from '@/services/branchStock'
import { useBranchStockStore } from './branchStock'
import type { Material } from '@/types/Material'
import type { BranchStock } from '@/types/BranchStock'
import { useSelectBranchStore } from './selectBranch'
import { useAuthStore } from './auth'

export const useCheckStockStore = defineStore('checkStock', () => {
  const materialStore = useMaterialStore()
  const checkStocks = ref<CheckStock[]>([])
  const employeeStore = useEmployeeStore()
  const branchStore = useBranchStore()
  const branchStockStore = useBranchStockStore()
  const checkStockDetails = ref<CheckStockDetail[]>([])
  const checkStockDialog = ref(false)
  const brachStockFoundBranches = ref<BranchStock[]>([])
  const selectBranchStore = useSelectBranchStore()
  const authStore = useAuthStore()

  const initialCheckStock = ref<CheckStock>({
    checkStockId: 0,
    date: '',
    employee: employeeStore.initaialEmployee,
    branch: branchStore.initialBranch,
    toggleStatus: false,
    checkstockDetails: []
  })

  const initialCheckStockDetail: CheckStockDetail = {
    checkStockDetailId: -1,
    checkStock: initialCheckStock.value,
    checkLast: 0,
    checkRemain: 0,
    used: 0,
    material: materialStore.initialMaterial,
    branchStock: branchStockStore.initialBranchStock
  }

  const editingCheckStockDetail = ref<CheckStockDetail>(
    JSON.parse(JSON.stringify(initialCheckStockDetail))
  )
  const editedCheckStock = ref<CheckStock>(JSON.parse(JSON.stringify(initialCheckStock)))
  const editedCheckStockDetail = ref<CheckStockDetail[]>([])

  const delLast = ref(false)
  watch(delLast, (newValue) => {
    const rollBackQty = ref<number[]>([])
    for (const item of checkStocks.value[checkStocks.value.length - 1].checkstockDetails) {
      rollBackQty.value.push(item.checkLast)
    }
    //materialStore.updateQty(rollBackQty.value)
    delLast.value = false
  })

  const confirmDelete = () => {
    delLast.value = true
  }

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }

  async function getCheckStock(id: number) {
    const res = await checkStockService.getCheckStock(id)
    editedCheckStock.value = res.data
  }

  async function getCheckStocks() {
    const res = await checkStockService.getCheckStocks()
    checkStocks.value = res.data
  }

  async function saveCheckStock() {
    console.log(editedCheckStock)
    const checkStockData = {
      checkStockId: 0,
      date: formatTime(new Date()),
      employee: authStore.getCurrentUser(),
      toggleStatus: false,
      branch: selectBranchStore.selectedBranch,
      checkstockDetails: editedCheckStockDetail.value
    }
    console.log(checkStockData)

    const impId = ref<number>(0)
    impId.value = checkStockData.checkStockId || 0
    if (checkStockData.checkStockId === 0) {
      const res = await checkStockService.addCheckStock(checkStockData)
    } else {
      console.log(impId.value)
      console.log(JSON.stringify(checkStockData))
      const res = await checkStockService.updateCheckStock(impId.value, checkStockData)
    }

    await getCheckStocks()
  }

  function findBranch() {
    const matchedBranchStock: BranchStock[] = []
    for (const branchStock of branchStockStore.branchStocks) {
      if (branchStock.branch.branchId === selectBranchStore.selectedBranch?.branchId) {
        matchedBranchStock.push(branchStock)
      }
    }
    brachStockFoundBranches.value.push(...matchedBranchStock)
    console.log(brachStockFoundBranches)
  }

  async function deleteCheckStock() {
    const checkStock = editedCheckStock.value
    const res = await checkStockService.delCheckStock(checkStock)
  }

  function clearForm() {
    editedCheckStock.value = JSON.parse(JSON.stringify(initialCheckStock))
  }

  const selectedEmployee = ref<string>()
  const selectedBranch = ref<string>()

  return {
    checkStocks,
    checkStockDialog,
    checkStockDetails,
    confirmDelete,
    getCheckStock,
    getCheckStocks,
    saveCheckStock,
    deleteCheckStock,
    clearForm,
    selectedBranch,
    selectedEmployee,
    findBranch,
    brachStockFoundBranches,
    editedCheckStock,
    initialCheckStockDetail,
    editingCheckStockDetail,
    editedCheckStockDetail
  }
})
