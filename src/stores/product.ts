import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'
import http from '@/services/http'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const Products = ref<Product[]>([])
  const productsForUnitSale = ref<Product[]>([])
  const pDescDialog = ref(false)
  const calenderDialog = ref(false)
  const unitSaleDialog = ref(false)
  const selectedMonth = ''
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]
  const selectedYear = ''
  const years = ['2024', '2023', '2022', '2021', '2020', '2019']

  async function getProducts() {
    Products.value = []
    productsDrink.value = []
    productsDessert.value = []
    productsFood.value = []
    const res = await productService.getProducts()
    for (const jsonData of res.data) {
      const itemProduct: Product = {
        productId: jsonData.productId,
        name: jsonData.name,
        image: jsonData.image,
        price: jsonData.price,
        category: { name: jsonData.category.name },
        qty: jsonData.qty
      }
      Products.value.push(itemProduct)
      if (itemProduct.category.name === 'Drink') {
        productsDrink.value.push(itemProduct)
      }
      if (itemProduct.category.name === 'Dessert') {
        productsDessert.value.push(itemProduct)
      }
      if (itemProduct.category.name === 'Food') {
        productsFood.value.push(itemProduct)
      }
    }
    // Products.value = res.data
    // return Products
  }

  async function getProductsForUnitSale() {
    const res = await productService.getProducts()
    productsForUnitSale.value = res.data
  }

  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0.0,
    category: { name: 'Drink' },
    image: 'noimage.jpg',
    files: [],
    qty: 0
  }

  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  //Data function
  async function getProduct(id: number) {
    loadingStore.doLoad()
    const res = await productService.getProduct(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }

  // async function getProducts() {
  //   loadingStore.doLoad()
  //   const res = await productService.getProducts()
  //   editedProduct.value = res.data
  //   loadingStore.finish()
  // }
  interface Cate {
    productcategoryId: number
  }
  const procatId = ref(0)
  function catid(cate: Cate) {
    if (editedProduct.value.category.name === 'Drink') {
      cate.productcategoryId = 1
    } else if (editedProduct.value.category.name === 'Dessert') {
      cate.productcategoryId = 2
    } else if (editedProduct.value.category.name === 'Food') {
      cate.productcategoryId = 3
    } else {
      cate.productcategoryId = 1
    }
  }
  async function saveProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value

    const cate = ref<Cate>({ productcategoryId: 0 })
    catid(cate.value)
    console.log('x')
    console.log(JSON.stringify(cate.value))

    if (!product.productId) {
      const formData = new FormData()
      formData.append('name', product.name)
      formData.append('price', product.price.toString())
      formData.append('category', JSON.stringify(cate.value))
      formData.append('qty', String(product.qty))

      if (image.value) {
        formData.append('image', image.value)
      }
      try {
        await http.post('http://localhost:3000/product', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        // alert('Data submitted successfully!')
      } catch (error) {
        console.error('Error submitting data:', error)
        alert('Error submitting data. Please try again.')
      }
    } else {
      //update
      const formData = new FormData()
      formData.append('name', product.name)
      formData.append('price', product.price.toString())
      formData.append('category', JSON.stringify(cate.value))
      formData.append('qty', String(product.qty))
      console.log(JSON.stringify(product.category))
      if (image.value) {
        formData.append('image', image.value)
      }
      try {
        await http.post(`http://localhost:3000/product/${product.productId}`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        console.log(product)
        // alert('Data submitted successfully!')
        // await productService.updateProduct(product)
      } catch (error) {
        console.error('Error submitting data:', error)
        alert('Error submitting data. Please try again.')
      }

      editDialog.value = false
    }
    // await getProducts()
    loadingStore.finish()
    image.value = null
  }
  function getImageUrl(imageFile: File | null) {
    if (imageFile) {
      console.log(URL.createObjectURL(imageFile))
      return URL.createObjectURL(imageFile)
    } else {
      // สร้าง URL สำหรับภาพเริ่มต้นที่นี่หรือใช้ URL ของภาพเริ่มต้น
      return 'http://localhost:3000/images/products/noimage.jpg'
    }
  }

  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)
    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }

  const productsDrink = ref<Product[]>([])
  const productsDessert = ref<Product[]>([])
  const productsFood = ref<Product[]>([])

  // const productDialog = ref(false)
  const image = ref<File | null>(null)
  const onFileChange = (event: Event) => {
    const input = event.target as HTMLInputElement
    if (input.files && input.files.length > 0) {
      image.value = input.files[0]
    }
    editDialog.value = false
  }
  const editDialog = ref(false)
  return {
    getProducts,
    saveProduct,
    deleteProduct,
    editedProduct,
    getProduct,
    clearForm,
    Products,
    productsDessert,
    productsDrink,
    productsFood,
    calenderDialog,
    selectedMonth,
    months,
    selectedYear,
    years,
    unitSaleDialog,
    productsForUnitSale,
    getProductsForUnitSale,
    image,
    onFileChange,
    getImageUrl,
    editDialog,
    pDescDialog
  }
})
