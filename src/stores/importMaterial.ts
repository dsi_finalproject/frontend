import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { ImportMaterial } from '@/types/ImportMaterial'
import type { ImportMaterialDetail } from '@/types/ImportMaterialDetail'
import importMaterialService from '@/services/importMaterial'
import { useEmployeeStore } from './employee'
import { useBranchStore } from './branch'
import { useMaterialStore } from './material'
import { useBranchStockStore } from './branchStock'
import { useSelectBranchStore } from './selectBranch'

export const useImportMaterialStore = defineStore('importMaterial', () => {
  const importMaterials = ref<ImportMaterial[]>([])
  const importMaterialDetails = ref<ImportMaterialDetail[]>([])
  const employeeStore = useEmployeeStore()
  const branchStore = useBranchStore()
  const materialStore = useMaterialStore()
  const selectBranchStore = useSelectBranchStore()
  const branchStockStore = useBranchStockStore()

  const initialImportMaterial = ref<ImportMaterial>({
    importMaterialId: 0,
    date: '',
    total: 0,
    totalList: 0,
    employee: employeeStore.initaialEmployee,
    branch: branchStore.initialBranch,
    toggleStatus: false,
    importMaterialDetails: []
  })

  const initialImportDetail: ImportMaterialDetail = {
    importMaterialDetailId: -1,
    importMaterial: initialImportMaterial.value,
    material: materialStore.initialMaterial,
    qty: 0,
    unitPrice: 0,
    total: 0,
    branchStock: branchStockStore.initialBranchStock
  }

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }

  const editedImportMaterial = ref<ImportMaterial>(
    JSON.parse(JSON.stringify(initialImportMaterial))
  )
  const editedImportMaterialDetail = ref<ImportMaterialDetail>(
    JSON.parse(JSON.stringify(initialImportDetail))
  )
  const editingimportDetails = ref<ImportMaterialDetail[]>([])

  async function getImportMaterial(id: number) {
    const res = await importMaterialService.getImportMaterial(id)
    editedImportMaterial.value = res.data
  }

  async function getImportMaterials() {
    const res = await importMaterialService.getImportMaterials()
    importMaterials.value = res.data
  }

  async function saveImportMaterial() {
    let totalSum = 0
    editingimportDetails.value.forEach((detail) => {
      totalSum += detail.total
    })
    const importMaterialData = {
      date: formatTime(new Date()),
      total: totalSum,
      totalList: editingimportDetails.value.length,
      employee: employeeStore.employees.find(
        (employee) => employee.name === selectedEmployee.value
      ),
      toggleStatus: editedImportMaterial.value.toggleStatus,
      branch: selectBranchStore.selectedBranch,
      importMaterialDetails: editingimportDetails.value
    }
    console.log(importMaterialData)

    const impId = ref<number>(0)
    impId.value = editedImportMaterial.value.importMaterialId || 0
    if (editedImportMaterial.value.importMaterialId === 0) {
      const res = await importMaterialService.addImportMaterial(importMaterialData)
    } else {
      console.log(impId.value)
      console.log(JSON.stringify(importMaterialData))
      const res = await importMaterialService.updateImportMaterial(impId.value, importMaterialData)
    }

    await getImportMaterials()
  }

  async function deleteImportMaterial() {
    const importMaterial = editedImportMaterial.value
    const res = await importMaterialService.delImportMaterial(importMaterial)
  }

  async function deleteImportMaterialDetail() {
    const detail = editedImportMaterialDetail.value
    const res = await importMaterialService.delImportMaterialDetail(detail)
  }

  const selectedEmployee = ref<string>()
  const selectedBranch = ref<string>()
  const selectedMaterial = ref<string>()

  function clearForm() {
    editedImportMaterial.value = JSON.parse(JSON.stringify(initialImportMaterial))
    selectedBranch.value = ''
    selectedEmployee.value = ''
  }

  function clearDetailForm() {
    selectedMaterial.value = ''
  }

  const lastID = 2
  const detailID = 0
  const importMaterialDialog = ref(false)
  const importMaterialDetailsDialog = ref(false)
  return {
    importMaterials,
    importMaterialDetails,
    importMaterialDialog,
    importMaterialDetailsDialog,
    editedImportMaterial,
    editingimportDetails,
    detailID,
    lastID,
    getImportMaterial,
    getImportMaterials,
    saveImportMaterial,
    deleteImportMaterial,
    clearForm,
    selectedEmployee,
    selectedBranch,
    selectedMaterial,
    initialImportMaterial,
    editedImportMaterialDetail,
    deleteImportMaterialDetail,
    initialImportDetail,
    clearDetailForm,
    formatTime
  }
})
