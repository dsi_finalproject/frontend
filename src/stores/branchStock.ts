import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { BranchStock } from '@/types/BranchStock'
import { useBranchStore } from './branch'
import { useMaterialStore } from './material'
import branchStockService from '@/services/branchStock'


export const useBranchStockStore = defineStore('branchStockStore', () => {
  const materialStore = useMaterialStore()
  const branchStore = useBranchStore()
  const bsEditId = ref(-1)
  const selectedBranch = ref<string>()
  const selectedMaterial = ref<string>()
  const branchStocks = ref<BranchStock[]>([])
  const initialBranchStock = ref<BranchStock>({
    branchStockId: -1,
    branch: branchStore.initialBranch,
    material: materialStore.initialMaterial,
    qty: 0
  })

  async function getBranchStock(id: number) {
    const res = await branchStockService.getBranchStock(id)
    editedBranchStock.value = res.data
  }

  async function getBranchStocks() {
    const res = await branchStockService.getBranchStocks()
    branchStocks.value = res.data
  }
  async function deleteBranchStock() {
    const branchStock = editedBranchStock.value
    const res = await branchStockService.deleteBranchStock(branchStock)

    await getBranchStocks()
  }
  const editedBranchStock = ref<BranchStock>(JSON.parse(JSON.stringify(initialBranchStock)))
  const branchstockDialog = ref(false)

  async function saveBranchStock() {
    const Data = {
      branch: branchStore.branches.find(
        (branch) => branch.name === selectedBranch.value
      ),
      material: materialStore.Materials.find((material) => material.name === selectedMaterial.value)
    }
    
      const res = await branchStockService.addBranchStock(Data)
    await getBranchStocks()
  }

  function clearForm() {
    editedBranchStock.value = JSON.parse(JSON.stringify(initialBranchStock))
    selectedBranch.value = ''
    selectedMaterial.value = ''
  }

  const branchName = ref('')
  const materialName = ref('')
  const qty = ref(0)

  return {
    branchStocks,
    editedBranchStock,
    initialBranchStock,
    branchstockDialog,
    getBranchStocks,
    deleteBranchStock,
    selectedBranch,
    selectedMaterial,
    clearForm,
    getBranchStock,
    branchName,
    materialName,
    qty,
    bsEditId,
    saveBranchStock
  }
})
