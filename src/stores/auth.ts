import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Employee } from '@/types/Employee'
import { useEmployeeStore } from './employee'
import authService from '@/services/auth'
import { useLoadingStore } from './loading'
import router from '@/router'
import { useLocateStore } from './locate'
import { useReceiptStore } from './receipt'
import { useBranchStore } from './branch'

export const useAuthStore = defineStore('auth', () => {
  const receiptStore = useReceiptStore()
  const warningDialog = ref(false)
  const loadingStore = useLoadingStore()
  const locateStore = useLocateStore()
  const branchStore = useBranchStore()
  // login เข้า (getuser)
  const login = async function (email: string, password: string) {
    try {
      loadingStore.doLoad()
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      router.replace('/')
      console.log(getCurrentUser())
      receiptStore.receiptQueue = 0
    } catch (e: any) {
      console.log(e)
    }
    locateStore.locate
    loadingStore.finish()
  }

  //Logout ล้าง User ออก
  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.replace('/login')
    console.log(getCurrentUser())
    receiptStore.receiptQueue = 0
    receiptStore.clearReceipt()
  }

  //ฟังก์ชั่นที่เก็บ user ไว้
  function getCurrentUser(): Employee {
    const strUser = localStorage.getItem('user')
    if (strUser !== null && strUser !== undefined) {
      const user = ref<Employee>()
      user.value = JSON.parse(strUser)
      if (user.value?.branch[0]) branchStore.currentBranch = user.value?.branch[0]
      return JSON.parse(strUser)
    } else {
      throw new Error('User not found in localStorage')
    }
  }

  //ฟังก์ชั่น get token ในการเข้าถึง
  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken == null) return null
    return JSON.parse(strToken)
  }

  return { warningDialog, login, getCurrentUser, getToken, logout }
})
