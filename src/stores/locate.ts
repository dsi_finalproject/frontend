import { ref, computed, onMounted, watch } from 'vue'
import { useRoute } from 'vue-router'
import { defineStore } from 'pinia'

export const useLocateStore = defineStore('locate', () => {
  const appBarLocate = ref('')
  const route = useRoute()

  const locations = [
    { url: '/', name: 'Main Menu' },
    { url: '/pos', name: 'Point Of Sale' },
    { url: '/receipthistory', name: 'Receipt History' },
    { url: '/checkstock', name: 'Check Stock Management' },
    { url: '/importmaterial', name: 'Import Material Management' },
    { url: '/branchstock', name: 'Branch Stock Management' },
    { url: '/material', name: 'Material Management' },
    { url: '/branch', name: 'Branch Management' },
    { url: '/promotion', name: 'Promotion Management' },
    { url: '/checkinout', name: 'Check In/Out Management' },
    { url: '/product', name: 'Product Management' },
    { url: '/employee', name: 'Employee Management' },
    { url: '/customer', name: 'Customer Management' },
    { url: '/salary', name: 'Salary Management' },
    { url: '/sliphistory', name: 'Payment Slip History' },
    { url: '/invoice', name: 'Utility Invoice Management' },
    { url: '/login', name: 'Log in' }
    // เพิ่ม URL และชื่อตามที่ต้องการ
  ]

  const locate = (place: string) => {
    appBarLocate.value = place
  }

  onMounted(() => {
    checkUrl()
  })

  // เช็ค URL ในหน้าปัจจุบัน
  const checkUrl = () => {
    const currentUrl = route.path
    const matchedLocation = locations.find((location) => location.url === currentUrl)
    if (matchedLocation) {
      appBarLocate.value = matchedLocation.name
    }
  }

  // ตรวจสอบ URL เมื่อมีการเปลี่ยนแปลง
  watch(route, () => {
    checkUrl()
  })

  return { appBarLocate, locate }
})
