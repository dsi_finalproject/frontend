import { type InvoiceDetail } from '@/types/InvoiceDetail'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Invoice } from '@/types/Invoice'
import type { Employee } from '@/types/Employee'
import type { Branch } from '@/types/Branch'
import branchService from '@/services/branch'
import employeeService from '@/services/employee'
import invoiceService from '@/services/utilityInvoice'
import { useAuthStore } from './auth'
import { useLoadingStore } from './loading'

export const useInvoiceStore = defineStore('invoice', () => {
  const editingInvoiceDetail = ref<InvoiceDetail[]>([])
  const invoiceDetail = ref<InvoiceDetail>({
    item: '',
    qty: 0,
    price: 0,
    total: 0,
    unit: ''
  })
  const invoiceDetailsDialog = ref(false)

  const invoice = ref<Invoice>({
    type: 'waterElectric',
    branch: {
      branchId: undefined,
      name: '',
      address: '',
      phone: ''
    },
    employee: {
      employeeId: undefined,
      name: '',
      email: '',
      password: '',
      roles: [],
      gender: '',
      salaryType: '',
      salary: undefined,
      phone: '',
      wagePerHour: 0,
      branch: []
    },
    total: 0,
    datePay: '',
    toggleStatus: false,
    invoiceDetails: []
  })

  const addDetail = () => {
    const clearIvDetail = ref<InvoiceDetail>({
      item: '',
      qty: 0,
      price: 0,
      total: 0,
      unit: ''
    })
    invoiceDetail.value.total = invoiceDetail.value.price * invoiceDetail.value.qty
    invoice.value.invoiceDetails.push(invoiceDetail.value)
    invoice.value.total += invoiceDetail.value.total

    invoiceDetail.value = clearIvDetail.value
  }

  const clear = () => {
    const clearInvoice = ref<Invoice>({
      invoiceId: 0,
      type: 'waterElectric',
      branch: {
        branchId: undefined,
        name: '',
        address: '',
        phone: ''
      },
      employee: {
        employeeId: undefined,
        name: '',
        email: '',
        password: '',
        roles: [],
        gender: '',
        salaryType: '',
        salary: undefined,
        phone: '',
        wagePerHour: 0,
        branch: []
      },
      total: 0,
      toggleStatus: false,
      invoiceDetails: []
    })
    invoice.value = clearInvoice.value
  }
  const loadingStore = useLoadingStore()
  async function getInvoice() {
    loadingStore.doLoad()
    const res = await invoiceService.getInvoices()
    invoices.value = res.data
    loadingStore.finish()
  }

  const invoiceDialog = ref(false)
  const authStore = useAuthStore()
  const branchName = ref('')
  const employeeName = ref('')

  const addInvoice = async () => {
    const empS = ref<Employee[]>([])
    const branchS = ref<Branch[]>([])
    const resE = await employeeService.getEmployees()
    const resB = await branchService.getBranchs()
    empS.value = resE.data
    branchS.value = resB.data
    employeeName.value = authStore.getCurrentUser().name
    console.log('EMPNAME' + employeeName.value)

    const emp: Employee = authStore.getCurrentUser()

    const br = ref<Branch>()
    const indexB = empS.value.findIndex((item) => (item.name = branchName.value))
    br.value = branchS.value[indexB]

    invoice.value.employee = emp
    invoice.value.branch = br.value
    invoice.value.datePay = formatTime(new Date())
    // console.log(JSON.stringify(invoice.value))
    invoiceService.addInvoice(invoice.value)
    const res = await invoiceService.getInvoices()
    invoices.value = res.data
    getInvoice()

    const clearInvoice = ref<Invoice>({
      invoiceId: 0,
      type: 'waterElectric',
      branch: {
        branchId: undefined,
        name: '',
        address: '',
        phone: ''
      },
      employee: {
        employeeId: undefined,
        name: '',
        email: '',
        password: '',
        roles: [],
        gender: '',
        salaryType: '',
        salary: undefined,
        phone: '',
        wagePerHour: 0,
        branch: []
      },
      total: 0,
      toggleStatus: false,
      invoiceDetails: []
    })

    invoice.value = clearInvoice.value
    addSuc.value = false
  }

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()

    return `${day}/${month}/${year} `
  }

  const invoices = ref<Invoice[]>([])

  const addSuc = ref(false)

  return {
    invoice,
    invoiceDetail,
    invoiceDetailsDialog,
    invoiceDialog,
    branchName,
    employeeName,
    invoices,
    editingInvoiceDetail,
    formatTime,
    addDetail,
    clear,
    addInvoice
  }
})
