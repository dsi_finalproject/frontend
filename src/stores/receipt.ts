import { ref, computed, watch, watchEffect, h } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import type { ReceiptDetail } from '@/types/ReceiptDetail'
import type { Customer } from '@/types/Customer'
import customerService from '@/services/customer'
import { useAuthStore } from './auth'
import { useBranchStore } from './branch'
import receiptService from '@/services/receipt'
import promotionService from '@/services/promotion'
import type { Promotion } from '@/types/Promotion'

export const useReceiptStore = defineStore('receipt', () => {
  const tempProduct = ref<Product>()
  const openOptionProduct = ref(false)

  const initialReceipt = ref<Receipt>({
    branch: {
      branchId: undefined,
      name: '',
      address: '',
      phone: ''
    },
    customer: {
      customerId: 0,
      name: '',
      phone: '',
      birthDate: '',
      point: 0
    },
    promotion: {
      promotionId: 0,
      name: '',
      status: 1,
      discountPercentage: undefined,
      discountBaht: undefined,
      usePoint: undefined,
      limitMenu: undefined
    },
    receiptDate: '',
    totalBefore: 0,
    discount: 0,
    total: 0,
    payment: 'Cash',
    birthDay: false,
    receiptDetail: []
  })

  const selectP = (qty: number, subCate: string, swtLevel: string, price: number) => {
    const tempReceiptItem = ref<ReceiptDetail>({
      qty: qty,
      price: price,
      total: price * qty,
      product: tempProduct.value!,
      productName: tempProduct.value?.name ?? 'Unknown',
      productCategory: tempProduct.value?.category.name,
      productSubCategory: subCate,
      productSweetLevel: swtLevel
    })

    let itemExists = false // Flag to check if item exists in the receipt detail

    for (const item of initialReceipt.value.receiptDetail) {
      if (
        tempReceiptItem.value.productName === item.productName &&
        tempReceiptItem.value.productSubCategory === item.productSubCategory &&
        tempReceiptItem.value.productSweetLevel === item.productSweetLevel
      ) {
        item.qty++
        itemExists = true
        calculateTotalBefore()
        break // Exit the loop since we found the item
      }
    }

    // ตรวจสอบว่า itemExists เป็นเท็จก่อนที่จะทำการ push ข้อมูลเข้าไปใน initialReceipt.value.receiptDetail
    if (!itemExists) {
      initialReceipt.value.receiptDetail.push(tempReceiptItem.value)
      calculateTotalBefore()
    }
  }

  const calculateTotalBefore = () => {
    initialReceipt.value.totalBefore = 0
    for (const item of initialReceipt.value.receiptDetail) {
      initialReceipt.value.totalBefore += item.price * item.qty
    }
    if (happybirthday.value === true) {
      initialReceipt.value.discount = parseFloat(
        (initialReceipt.value.totalBefore * bdPercentage.value).toFixed(2)
      )
    }
    if (proUsed.value !== null) {
      if (proUsed.value?.discountBaht)
        initialReceipt.value.discount += parseFloat(proUsed.value?.discountBaht.toFixed(2))
      if (proUsed.value?.discountPercentage)
        initialReceipt.value.discount += parseFloat(
          (initialReceipt.value.total * (proUsed.value?.discountPercentage / 100)).toFixed(2)
        )
    }
    initialReceipt.value.total = parseFloat(
      (initialReceipt.value.totalBefore - initialReceipt.value.discount).toFixed(2)
    )
    if (currentCustomer.value !== null) {
      calPoint()
    }
  }

  const promotions = ref<Promotion[]>([])
  const proUsed = ref<Promotion>()

  const removeReceiptItem = (item: ReceiptDetail) => {
    const index = initialReceipt.value.receiptDetail.findIndex(
      (receiptItem) => receiptItem === item
    )
    initialReceipt.value.receiptDetail.splice(index, 1)
  }

  const receiptQueue = ref(0)
  const selectedPayment = ref<string>('Cash')
  const receiveMoney = ref<number>(0)

  const currentCustomer = ref<Customer>()
  const customers = ref<Customer[]>([])
  const getPoint = ref<number>(0)
  const calPoint = () => {
    getPoint.value = 0
    for (const item of initialReceipt.value.receiptDetail) {
      getPoint.value += item.qty
    }
  }
  const searchCustomer = async (tel: string) => {
    const addedReceipts = ref<Receipt[]>([])
    const res = await customerService.getCustomers()
    addedReceipts.value = res.data

    customers.value = res.data
    const index = customers.value.findIndex((customer) => customer.phone === tel)
    currentCustomer.value = customers.value[index]

    if (currentCustomer.value?.point) tempPoint.value = currentCustomer.value?.point

    const dateNow = new Date()
    const birthDateParts = currentCustomer?.value.birthDate.split('-') // แยกวันเกิดออกเป็นส่วน
    const birthDay = birthDateParts ? parseInt(birthDateParts[2], 10) : 0 // วันที่
    const birthMonth = birthDateParts ? parseInt(birthDateParts[1], 10) : 0

    const promotionsRes = await promotionService.getPromotions()
    const promotions = promotionsRes.data

    if (birthDay === dateNow.getDate() && birthMonth === dateNow.getMonth() + 1) {
      happybirthday.value = true
      bdPercentage.value = promotions[0].discountPercentage / 100
      calculateTotalBefore()
    }
    initialReceipt.value.customer = currentCustomer.value
  }

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }
  const receiptDialog = ref(false)
  const authStore = useAuthStore()
  const branchStore = useBranchStore()
  const savedReceipt = ref<Receipt>()
  const saveReceipt = async () => {
    initialReceipt.value.queue = ++receiptQueue.value
    initialReceipt.value.receiptDate = formatTime(new Date())
    initialReceipt.value.total = initialReceipt.value.totalBefore - initialReceipt.value.discount
    initialReceipt.value.receiveAmount = receiveMoney.value
    initialReceipt.value.change = initialReceipt.value.receiveAmount - initialReceipt.value.total
    if (selectedPayment.value === 'Promptpay' || selectedPayment.value === 'Cash') {
      initialReceipt.value.payment = selectedPayment.value
    }

    initialReceipt.value.birthDay = happybirthday.value

    initialReceipt.value.employee = authStore.getCurrentUser()
    initialReceipt.value.branch = branchStore.currentBranch
    initialReceipt.value.getPoint = getPoint.value
    initialReceipt.value.customerPointBefore = currentCustomer.value?.point

    if (proUsed.value) initialReceipt.value.promotion = proUsed.value
    {
      initialReceipt.value.customer.point += getPoint.value
      initialReceipt.value.customer.point -= initialReceipt.value.promotion.usePoint ?? 0
      customerService.updateCustomer(initialReceipt.value.customer)
    }

    descriptReceipt.value = initialReceipt.value
    receiptDialog.value = true
    await receiptService.addReceipt(initialReceipt.value)

    clearReceipt()
  }

  const descriptReceipt = ref<Receipt>()

  const tempPoint = ref<number>(0)
  const promotionDialog = ref(false)
  const happybirthday = ref(false)
  const bdPercentage = ref<number>(0)

  const ppDialog = ref(false)

  const clearReceipt = () => {
    const cleanReceipt = ref<Receipt>({
      branch: {
        branchId: undefined,
        name: '',
        address: '',
        phone: ''
      },
      customer: {
        customerId: 0,
        name: '',
        phone: '',
        birthDate: '',
        point: 0
      },
      promotion: {
        promotionId: 0,
        name: '',
        status: 1,
        discountPercentage: undefined,
        discountBaht: undefined,
        usePoint: undefined,
        limitMenu: undefined
      },
      receiptDate: '',
      totalBefore: 0,
      discount: 0,
      total: 0,
      payment: 'Cash',
      receiptDetail: [],
      birthDay: false
    })
    initialReceipt.value = cleanReceipt.value
    tempProduct.value = undefined
    selectedPayment.value = 'Cash'
    receiveMoney.value = 0
    currentCustomer.value = undefined
    tempPoint.value = 0
    happybirthday.value = false
    bdPercentage.value = 0
    proUsed.value = undefined
    getPoint.value = 0
  }

  const receipts = ref<Receipt[]>([])

  return {
    tempProduct,
    openOptionProduct,
    initialReceipt,
    receiptQueue,
    selectedPayment,
    receiveMoney,
    currentCustomer,
    savedReceipt,
    tempPoint,
    promotionDialog,
    happybirthday,
    bdPercentage,
    promotions,
    proUsed,
    getPoint,
    ppDialog,
    descriptReceipt,
    receiptDialog,
    receipts,
    selectP,
    removeReceiptItem,
    calculateTotalBefore,
    searchCustomer,
    saveReceipt,
    formatTime,
    clearReceipt
  }
})
