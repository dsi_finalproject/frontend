import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { CheckInOut } from '@/types/CheckInOut'
import type { Employee } from '@/types/Employee'
import type { Salary } from '@/types/Salary'
import salaryService from '@/services/salary'
import employeeService from '@/services/employee'

export const useSalaryStore = defineStore('salary', () => {
  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }

  const createSalaryDialog = ref(false)
  const selectChkIOs = ref<CheckInOut[]>()
  const selectEmployee = ref<string>()

  const initialSalary = ref<Salary>({
    checkinout: [],
    employee: {
      employeeId: undefined,
      name: '',
      email: '',
      password: '',
      roles: [],
      gender: '',
      salaryType: '',
      salary: undefined,
      phone: '',
      wagePerHour: 0,
      branch: []
    },
    salaryCreate: '',
    salaryPay: '',
    salaryTotal: 0,
    salaryStatus: 'Not Paid',
    branch: {
      branchId: undefined,
      name: '',
      address: '',
      phone: ''
    }
  })

  const createSalary = () => {
    // console.log(JSON.stringify(initialSalary.value))
    salaryService.addsalary(initialSalary.value)
  }

  const clear = () => {
    const cslr = ref<Salary>({
      checkinout: [],
      employee: {
        employeeId: undefined,
        name: '',
        email: '',
        password: '',
        roles: [],
        gender: '',
        salaryType: '',
        salary: undefined,
        phone: '',
        wagePerHour: 0,
        branch: []
      },
      salaryCreate: '',
      salaryPay: '',
      salaryTotal: 0,
      salaryStatus: 'Not Paid',
      branch: {
        branchId: undefined,
        name: '',
        address: '',
        phone: ''
      }
    })
    initialSalary.value = cslr.value
  }

  const salaries = ref<Salary[]>([])

  const update = async (updatePoint: Salary) => {
    const item = ref<Salary>({
      checkinout: [],
      employee: {
        employeeId: undefined,
        name: '',
        email: '',
        password: '',
        roles: [],
        gender: '',
        salaryType: '',
        salary: undefined,
        phone: '',
        wagePerHour: 0,
        branch: []
      },
      salaryCreate: '',
      salaryPay: '',
      salaryTotal: 0,
      salaryStatus: 'Not Paid',
      branch: {
        branchId: undefined,
        name: '',
        address: '',
        phone: ''
      }
    })

    item.value.salaryId = updatePoint.salaryId
    item.value.salaryPay = formatTime(new Date())
    item.value.salaryStatus = 'Paid'
    item.value.salaryCreate = updatePoint.salaryCreate
    salaryService.updatesalary(item.value)
    const res = await salaryService.getsalary(item.value.salaryId ?? 0)
    const Salari = ref<Salary>()
    Salari.value = res.data
    showSalary.value = item.value
    if (Salari.value !== undefined) showSalary.value = Salari.value

    showSalary.value.employee = updatePoint.employee
    const res2 = await employeeService.getEmployees()
    const emps = ref<Employee[]>([])
    emps.value = res2.data
    const index = emps.value.findIndex((item) => item.name === showSalary.value.employee.name)
    showSalary.value.employee = emps.value[index]
    showSalary.value.salaryPay = formatTime(new Date())
    // showSalary.value.salaryPay = item.value.salaryPay
    slipDialog.value = true
    // showSalary.value = item.value
    // console.log(updatePoint.employee)
    // showSalary.value.employee.name = updatePoint.employee.name
    // showSalary.value.employee.roles.push(updatePoint.employee.roles[0])
    // showSalary.value.salaryTotal = updatePoint.salaryTotal
    // location.reload()
  }

  const slipDialog = ref(false)
  const showSalary = ref<Salary>({
    checkinout: [],
    employee: {
      employeeId: undefined,
      name: '',
      email: '',
      password: '',
      roles: [],
      gender: '',
      salaryType: '',
      salary: undefined,
      phone: '',
      wagePerHour: 0,
      branch: []
    },
    salaryCreate: '',
    salaryPay: '',
    salaryTotal: 0,
    salaryStatus: 'Paid',
    branch: {
      branchId: undefined,
      name: '',
      address: '',
      phone: ''
    }
  })

  const seeHis = (item: Salary) => {
    showSalary.value = item
    console.log(showSalary.value)
    hisSlip.value = true
  }
  const hisSlip = ref(false)

  return {
    createSalaryDialog,
    selectChkIOs,
    selectEmployee,
    initialSalary,
    salaries,
    slipDialog,
    showSalary,
    hisSlip,
    clear,
    formatTime,
    createSalary,
    update,
    seeHis
  }
})
