import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'
import PromotionService from '@/services/promotion'
import { useLoadingStore } from './loading'
export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const promotions = ref<Promotion[]>([])
  const initialPromotion: Promotion = {
    promotionId: -1,
    name: '',
    status: 0,
    discountPercentage: 0,
    discountBaht: 0,
    usePoint: 0,
    limitMenu: 0
  }
  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))
  async function getPromotion(id: number) {
    loadingStore.doLoad()
    const res = await PromotionService.getPromotion(id)
    editedPromotion.value = res.data
    loadingStore.finish()
  }
  async function getPromotions() {
    loadingStore.doLoad()
    const res = await PromotionService.getPromotions()
    promotions.value = res.data
    loadingStore.finish()
  }

  async function savePromotion() {
    loadingStore.doLoad()
    const promotion = editedPromotion.value
    if (promotion.promotionId === -1) {
      const pb = ref<Promotion>({
        name: promotion.name,
        status: promotion.status,
        discountPercentage: promotion.discountPercentage,
        discountBaht: promotion.discountBaht,
        usePoint: promotion.usePoint,
        limitMenu: promotion.limitMenu
      })

      promotion.promotionId === null
      console.log('Post ' + JSON.stringify(pb))
      const res = await PromotionService.addPromotion(pb.value)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(promotion))
      const res = await PromotionService.updatePromotion(promotion)
    }

    await getPromotions()
    loadingStore.finish()
  }

  async function updateStatus(promotionId: number, status: number) {
    loadingStore.doLoad()
    await PromotionService.updatePromotionStatus(promotionId, status)
    loadingStore.finish()
  }

  async function deletePromotion() {
    const Promotion = editedPromotion.value
    loadingStore.doLoad()
    const res = await PromotionService.delPromotion(Promotion)
    loadingStore.finish()

    await getPromotions()
  }

  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
  }

  // const promotionDialog = ref(false)

  return {
    promotions,
    getPromotion,
    savePromotion,
    deletePromotion,
    clearForm,
    initialPromotion,
    editedPromotion,
    getPromotions,
    updateStatus
  }
})
