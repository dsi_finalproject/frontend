import { ref, computed } from 'vue'
import type { Customer } from '@/types/Customer'
import { usePromotionStore } from './promotion'
import { useReceiptStore } from './receipt'
import CustomerService from '@/services/customer'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'

export const useCustomerStore = defineStore('customer', () => {
  const customers = ref<Customer[]>([])
  const loadingStore = useLoadingStore()
  const initialCustomer = ref<Customer>({
    customerId: -1,
    name: '',
    phone: '',
    birthDate: '',
    point: 0
  })
  async function getCustomer(id: number) {
    const res = await CustomerService.getCustomer(id)
    editedCustomer.value = res.data
  }
  async function getCustomers() {
    loadingStore.doLoad()
    const res = await CustomerService.getCustomers()
    customers.value = res.data
    loadingStore.finish()
  }
  async function saveCustomer() {
    const customer = editedCustomer.value
    if (customer.customerId === -1) {
      const cb = ref<Customer>({
        name: customer.name,
        phone: customer.phone,
        birthDate: customer.birthDate,
        point: customer.point,
        customerId: 0
      })

      customer.customerId === null
      console.log('Post ' + JSON.stringify(cb))
      const res = await CustomerService.addCustomer(cb.value)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(customer))
      const res = await CustomerService.updateCustomer(customer)
    }

    await getCustomers()
  }

  async function deleteCustomer() {
    const Branch = editedCustomer.value
    const res = await CustomerService.delCustomer(Branch)

    await getCustomers()
  }

  const search = async (phone: string) => {
    try {
      const response = await CustomerService.getCustomers()
      const customersData = response.data // Extracting the data from AxiosResponse
      customers.value = customersData
      const cust = customers.value.find((customer) => customer.phone === phone)
      takePoint.value = true
      if (cust !== undefined) {
        currentCustomer.value = cust
      }
    } catch (error) {
      console.error('Error fetching customers:', error)
      // Handle error appropriately
    }
  }

  const currentCustomer = ref<Customer>({
    customerId: 0,
    name: '',
    phone: '',
    birthDate: '',
    point: 0
  })

  function clearForm() {
    editedCustomer.value = JSON.parse(JSON.stringify(initialCustomer))
  }
  const editedCustomer = ref<Customer>(JSON.parse(JSON.stringify(initialCustomer)))
  const takePoint = ref(false)

  const telInput = ref('')

  const promotionStore = usePromotionStore()
  const receiptStore = useReceiptStore()

  function calculateDiscount() {
    const dateNow = new Date()
    const birthDateParts = currentCustomer.value.birthDate.split('-') // แยกวันเกิดออกเป็นส่วน
    const birthDay = parseInt(birthDateParts[0], 10) // วันที่
    const birthMonth = dateNow.getMonth() + 1 // เดือน (เพิ่ม 1 เนื่องจาก getMonth() เริ่มต้นที่ 0)

    // เช็คว่าวันเกิดเท่ากับวันปัจจุบันหรือไม่
    if (birthDay === dateNow.getDate() && birthMonth === dateNow.getMonth() + 1) {
      if (promotionStore.promotions[0].discountPercentage)
        bdPercentage.value = promotionStore.promotions[0].discountPercentage / 100
      // receiptStore.bdPercentage = bdPercentage.value
    }
  }

  const customerDialog = ref(false)
  const bdPercentage = ref(0)
  const cusDialog = ref(false)
  //แนะนำว่าควรมี function ที่แปลงการแอดวันเดือนปีเกิดแบบตัวหนังสือให้เป็นตัวเลขเพื่อทำการเก็บ และมีfunctionแปลงตัวเลขไปเป็นตัวหนังสือเพื่อทำการแสดงผลบนตาราง
  //birthDate : MM-DD

  return {
    customers,
    search,
    currentCustomer,
    takePoint,
    telInput,
    calculateDiscount,
    clearForm,
    bdPercentage,
    customerDialog,
    saveCustomer,
    getCustomer,
    getCustomers,
    deleteCustomer,
    editedCustomer,
    initialCustomer,
    cusDialog
  }
})
