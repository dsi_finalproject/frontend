import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { type Branch } from '@/types/Branch'
import BranchService from '@/services/branch'

export const useBranchStore = defineStore('branch', () => {
  const branches = ref<Branch[]>([])
  const initialBranch = ref<Branch>({
    branchId: 0,
    name: '',
    address: '',
    phone: ''
  })

  const currentBranch = ref<Branch>({
    name: 'บางแสน',
    address: 'xxx xxx xxx',
    phone: '081'
  })

  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  async function getBranch(id: number) {
    const res = await BranchService.getBranch(id)
    editedBranch.value = res.data
  }
  async function getBranches() {
    const res = await BranchService.getBranchs()
    branches.value = res.data
  }
  async function saveBranch() {
    const branch = editedBranch.value
    if (branch.branchId === 0) {
      const nb = ref<Branch>({
        name: branch.name,
        address: branch.address,
        phone: branch.phone
      })

      branch.branchId === null
      console.log('Post ' + JSON.stringify(nb))
      const res = await BranchService.addBranch(nb.value)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(branch))
      const res = await BranchService.updateBranch(branch)
    }

    await getBranches()
  }
  async function deleteBranch() {
    const Branch = editedBranch.value
    const res = await BranchService.delBranch(Branch)

    await getBranches()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  const branchDialog = ref(false)

  return {
    branches,
    getBranches,
    getBranch,
    saveBranch,
    deleteBranch,
    clearForm,
    editedBranch,
    branchDialog,
    initialBranch,
    currentBranch
  }
})
