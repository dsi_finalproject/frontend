import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Employee } from '@/types/Employee'
import { useLoadingStore } from './loading'
import employeeService from '@/services/employee'
import type { Role } from '@/types/Role'
import roleService from '@/services/role'
import { useRoleStore } from './role'
import branchService from '@/services/branch'
import { useBranchStore } from './branch'
import type { Branch } from '@/types/Branch'

export const useEmployeeStore = defineStore('employee', () => {
  const loadingStore = useLoadingStore()
  const employees = ref<Employee[]>([])
  const monthselldialog = ref(false)
  const initaialEmployee = ref<Employee>({
    // employeeId: 0,
    name: '',
    email: '',
    password: '',
    roles: [],
    gender: 'male',
    salaryType: 'Full-Time',
    phone: '',
    wagePerHour: 0,
    branch: []
  })
  const selectedMonth = ''
  const months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
  const selectedYear = ''
  const years = ['2024', '2023', '2022', '2021', '2020', '2019']
  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initaialEmployee)))
  const rolesEdit = ref<Role[]>([])

  async function getEmployee(id: number) {
    const res = await employeeService.getEmployee(id)
    editedEmployee.value = res.data
  }

  async function getEmployees() {
    loadingStore.doLoad()
    const res = await employeeService.getEmployees()
    employees.value = res.data
    loadingStore.finish()
  }

  const empEditId = ref(-1)
  const branchStore = useBranchStore()
  const roleStore = useRoleStore()
  async function saveEmployee() {
    console.log(selectedBranch)
    const employeeData = ref<Employee>({
      name: empName.value,
      email: empEmail.value,
      password: empPass.value,
      gender: selectedGender.value ?? '',
      salaryType: selectedSalaryType.value ?? '',
      salary: empSalary.value,
      phone: empPhone.value,
      wagePerHour: empWage.value,
      roles: [],
      branch: []
    })

    if (selectedBranch.value !== null) {
      for (const ib of selectedBranch.value) {
        const index = branchStore.branches.findIndex((item) => item.name === ib)
        const b = branchStore.branches[index]
        employeeData.value.branch.push(b)
      }
    }
    if (selectedRoles.value !== null) {
      for (const ir of selectedRoles.value) {
        const index = roleStore.roles.findIndex((item) => item.name === ir)
        const r = roleStore.roles[index]
        employeeData.value.roles.push(r)
      }
    }
    console.log('CheckE')
    console.log(JSON.stringify(employeeData.value))
    if (empEditId.value === -1) {
      console.log(JSON.stringify(employeeData.value))
      console.log('ถุง')
      const res = employeeService.addEmployee(employeeData.value)
      const resx = await employeeService.getEmployees()
      employees.value = resx.data
      getEmployees()
    } else {
      employeeData.value.employeeId = editedEmployee.value.employeeId ?? 0
      console.log('ถึง')
      console.log(JSON.stringify(employeeData.value))
      const res = employeeService.updateEmployee(employeeData.value.employeeId, employeeData.value)
      const resx = await employeeService.getEmployees()
      employees.value = resx.data
      getEmployees()
    }

    selectedRoles.value = []
    selectedBranch.value = []
    selectedSalaryType.value = ''
    empChange.value = !empChange.value
    empEmail.value = ''
    empName.value = ''
    empPass.value = ''
    empSalary.value = 0
    empPhone.value = ''
    empWage.value = 0
  }

  const empChange = ref(false)

  async function deleteEmployee() {
    const employee = editedEmployee.value
    const res = await employeeService.delEmployee(employee)
    const resx = await employeeService.getEmployees()
    employees.value = resx.data
    getEmployees()
  }

  function clearForm() {
    editedEmployee.value = JSON.parse(JSON.stringify(initaialEmployee))
    selectedRoles.value = []
    selectedBranch.value = []
    selectedSalaryType.value = ''
    empChange.value = !empChange.value
    empEmail.value = ''
    empName.value = ''
    empPass.value = ''
    empSalary.value = 0
    empPhone.value = ''
    empWage.value = 0
  }
  const employeeDialog = ref(false)
  const selectedRoles = ref<string[]>([])
  const selectedBranch = ref<string[]>([])
  const selectedSalaryType = ref<string>()
  const selectedGender = ref<string>()

  const empEmail = ref('')
  const empName = ref('')
  const empPass = ref('')
  const empSalary = ref(0)
  const empPhone = ref('')
  const empWage = ref(0)

  return {
    getEmployee,
    getEmployees,
    saveEmployee,
    deleteEmployee,
    clearForm,
    employees,
    initaialEmployee,
    editedEmployee,
    employeeDialog,
    selectedRoles,
    selectedBranch,
    selectedSalaryType,
    selectedGender,
    empEmail,
    empName,
    empPass,
    empSalary,
    empPhone,
    empWage,
    empEditId,
    selectedMonth,
    selectedYear,
    months,
    years,
    monthselldialog
  }
})
