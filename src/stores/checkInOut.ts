import { ref, computed, onMounted } from 'vue'
import { defineStore } from 'pinia'
import type { CheckInOut } from '@/types/CheckInOut'
import type { Employee } from '@/types/Employee'
import checkInOutService from '@/services/checkInOut'
import { useAuthStore } from './auth'

export const useCheckInOutStore = defineStore('checkinout', () => {
  const checkinout = ref<CheckInOut[]>([])
  const checkId = ref<number>(parseInt(localStorage.getItem('checkId') || '0')) // ดึงค่า checkId จาก localStorage หากมี

  async function getCheckInOuts() {
    const res = await checkInOutService.getAll()
    checkinout.value = res.data
  }

  function convertStringToDateTime(dateString: string): Date {
    // แยกวันที่และเวลาออกจากกัน
    const [datePart, timePart] = dateString.split(' ')
    const [day, month, year] = datePart.split('/')
    const [hour, minute, second] = timePart.split(':')

    // สร้างวัตถุ Date จากวันที่และเวลาที่แยกออกมา
    const dateObject = new Date(
      parseInt(year),
      parseInt(month) - 1,
      parseInt(day),
      parseInt(hour),
      parseInt(minute),
      parseInt(second)
    )

    return dateObject
  }

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`
  }
  function getHourDifference(date1: Date, date2: Date): number {
    const timeDifference = date2.getTime() - date1.getTime()
    const hourDifference = timeDifference / (1000 * 60 * 60)
    // const roundedDifference = Number(hourDifference.toFixed(2)) // ปัดผลลัพธ์ให้ไม่เกินทศนิยมสองตำแหน่ง
    return Math.abs(hourDifference)
  }

  const authStore = useAuthStore()
  const checkIn = async (emp: Employee) => {
    const newCheckInOut = {
      checkIn: formatTime(new Date()),
      employee: authStore.getCurrentUser()
    }
    // JSON.stringify(newCheckInOut)
    const res = await checkInOutService.checkIn(newCheckInOut)
    getCheckInOuts()
    checkId.value = res.data.checkInOutId
    localStorage.setItem('checkId', checkId.value.toString()) // บันทึกค่า checkId ใน localStorage
  }

  const checkOut = async (emp: Employee) => {
    const item = await checkInOutService.getOne(checkId.value)

    if (emp.employeeId !== 0 && item.data !== undefined) {
      item.data.checkOut = formatTime(new Date())
      const chkIn = convertStringToDateTime(item.data.checkIn)
      const chkOut = convertStringToDateTime(item.data.checkOut)
      const timeWork = getHourDifference(chkIn, chkOut)
      item.data.timeWork = timeWork

      let status = 0

      if (emp.salaryType === 'Full-Time') {
        const checkInHour = chkIn.getHours()
        const checkOutHour = chkOut.getHours()

        if (checkInHour < 9 && checkOutHour >= 21) {
          status = 1
        } else if (checkInHour >= 9) {
          status = 2
        } else if (checkOutHour < 21) {
          status = 3
        } else {
          status = 5
        }
      } else if (emp.salaryType === 'Part-Time') {
        if (timeWork >= 4) {
          status = 1
        } else {
          status = 6
        }
      }
      item.data.status = status
      await checkInOutService.checkOut(item.data)
      getCheckInOuts()

      // const index = checkinout.value.findIndex((element) => element.id === item.id)
      // if (index !== -1) {
      //   checkinout.value[index] = item
      // }
    }
  }

  return { checkinout, checkIn, checkOut, getCheckInOuts }
})
