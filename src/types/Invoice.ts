import type { Branch } from './Branch'
import type { Employee } from './Employee'
import type { InvoiceDetail } from './InvoiceDetail'

type Invoice = {
  invoiceId?: number
  type: 'waterElectric' | 'material' | 'ค่าใช้จ่ายต่างๆ'
  branch: Branch
  employee: Employee
  total: number
  datePay?: string
  toggleStatus: boolean
  invoiceDetails: InvoiceDetail[]
}

export { type Invoice }
