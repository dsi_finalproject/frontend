type WorkTime = {
  workTimeId: number
  employeeId: number
  totalTime?: number
  overTime?: number
}

export { type WorkTime }
