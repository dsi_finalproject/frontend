import type { Branch } from './Branch'
import type { Employee } from './Employee'
import type { ImportMaterialDetail } from './ImportMaterialDetail'
type ImportMaterial = {
  importMaterialId: number
  date: string
  total: number
  totalList: number
  employee: Employee
  branch: Branch
  toggleStatus: boolean
  importMaterialDetails: ImportMaterialDetail[]
}

export { type ImportMaterial }
