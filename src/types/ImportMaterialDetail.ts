import type { BranchStock } from './BranchStock'
import type { ImportMaterial } from './ImportMaterial'
import type { Material } from './Material'

type ImportMaterialDetail = {
  importMaterialDetailId: number
  importMaterial: ImportMaterial
  material: Material
  qty: number
  unitPrice: number
  total: number
  branchStock: BranchStock
}

export { type ImportMaterialDetail }
