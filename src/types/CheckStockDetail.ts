import type { BranchStock } from './BranchStock'
import type { CheckStock } from './CheckStock'
import type { Material } from './Material'

type CheckStockDetail = {
  checkStockDetailId: number
  material: Material
  checkLast: number
  checkRemain: number
  used: number
  checkStock: CheckStock
  branchStock: BranchStock
}

export { type CheckStockDetail }
