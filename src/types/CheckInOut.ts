import type { Employee } from './Employee'

type CheckInOut = {
  checkInOutId?: number
  checkIn: string
  checkOut?: string
  timeWork?: number
  status?: number
  employee?: Employee
  used: number
}

export { type CheckInOut }
