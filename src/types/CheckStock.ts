import type { Branch } from './Branch'
import type { CheckStockDetail } from './CheckStockDetail'
import type { Employee } from './Employee'
type CheckStock = {
  checkStockId: number
  date: string
  employee: Employee
  branch: Branch
  toggleStatus: boolean
  checkstockDetails: CheckStockDetail[]
}

export { type CheckStock }
