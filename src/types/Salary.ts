import type { Branch } from './Branch'
import type { CheckInOut } from './CheckInOut'
import type { Employee } from './Employee'

type Salary = {
  salaryId?: number
  checkinout: CheckInOut[]
  employee: Employee
  salaryCreate: string
  salaryPay: string
  salaryTotal: number
  salaryStatus: 'Paid' | 'Not Paid'
  branch: Branch
}

export { type Salary }
