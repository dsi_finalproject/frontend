type Promotion = {
  promotionId?: number
  name: string
  status: number
  discountPercentage?: number
  discountBaht?: number
  usePoint?: number
  limitMenu?: number
}

export { type Promotion }
