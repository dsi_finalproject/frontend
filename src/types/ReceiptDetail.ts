import type { Product } from './Product'

type ReceiptDetail = {
  receiptDetailId?: number
  receiptId?: number
  qty: number
  price: number
  total: number
  product: Product
  productName: string
  productCategory?: string
  productSubCategory?: string //'เย็น' | 'ร้อน' | 'ปั่น'
  productSweetLevel?: string //'0%' | '25%' | '50%' | '75%' | '100% '
}

export { type ReceiptDetail }
