type InvoiceDetail = {
  invoiceDetailId?: number
  invoiceId?: number
  item: string
  qty: number
  price: number
  total: number
  unit: string
  // material_id?: number
}

export { type InvoiceDetail }
