type Material = {
  materialId?: number
  name: string
  price: number
  minimum: number
  unit: string
}

export { type Material }
