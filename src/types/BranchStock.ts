import type { Branch } from './Branch'
import type { Material } from './Material'

type BranchStock = {
  branchStockId?: number
  branch: Branch
  material: Material
  qty: number
}

export { type BranchStock }
