import type { Branch } from './Branch'
import type { Customer } from './Customer'
import type { Employee } from './Employee'
import type { Promotion } from './Promotion'
import type { ReceiptDetail } from './ReceiptDetail'

type Receipt = {
  receiptId?: number
  branch: Branch
  employee?: Employee | null
  customer: Customer
  promotion: Promotion
  queue?: number
  receiptDate: string
  totalBefore: number
  discount: number
  total: number
  birthDay: boolean 
  receiveAmount?: number
  change?: number
  payment: 'Cash' | 'Promptpay'
  getPoint?: number
  customerPointBefore?: number
  receiptDetail: ReceiptDetail[]
}

export { type Receipt }
