type Product = {
  productId?: number
  name: string
  price: number
  image: string
  category: { name: 'Drink' | 'Food' | 'Dessert'; productcategoryId?: number }
  subCategory?: number //'เย็น' | 'ร้อน' | 'ปั่น'
  sweetLevel?: number //'0%' | '25%' | '50%' | '75%' | '100% '
  qty?: number //จำนวนคงเหลือ
}
function getImageUrl(product: Product) {
  return `http://localhost:3000/images/products/${product.image}`
}

export { type Product, getImageUrl }
