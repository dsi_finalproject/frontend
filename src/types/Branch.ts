type Branch = {
  branchId?: number
  name: string
  address: string
  phone: string
}

export { type Branch }
