import type { Branch } from './Branch'
import type { Role } from './Role'

type Employee = {
  employeeId?: number
  name: string
  email: string
  password: string
  roles: Role[]
  gender: string
  salaryType: string
  salary?: number
  phone: string
  wagePerHour: number
  branch: Branch[]
}

export { type Employee }
