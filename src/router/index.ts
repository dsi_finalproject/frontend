import { useAuthStore } from '@/stores/auth'
import type { Employee } from '@/types/Employee'
import { ref } from 'vue'
import { createRouter, createWebHistory, type NavigationGuardNext, type RouteLocationNormalized, type RouteLocationNormalizedLoaded } from 'vue-router'

const requireRole = (to: RouteLocationNormalized, from: RouteLocationNormalizedLoaded, next: NavigationGuardNext) => {
  if (emp.value?.roles[0].roleId == 3 && to.path === '/invoice' ) {
    next(false);
  } else if(emp.value?.roles[0].roleId == 3 && to.path === '/salary'){
    next(false);
  } else if(emp.value?.roles[0].roleId == 3 && to.path === '/branch'){
    next(false);
  } else if(emp.value?.roles[0].roleId == 3 && to.path === '/employee'){
    next(false);
  }else{
    next()
  }
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'mainmenu',
      component: () => import('../views/MainMenuView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },
    {
      path: '/pos',
      name: 'pos',
      component: () => import('../views/POS/posView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/checkinout',
      name: 'checkinout',
      component: () => import('../views/CheckInOut/CheckInOut.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/receipthistory',
      name: 'receipthistory',
      component: () => import('@/views/POS/ReceiptHistoryView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/branch',
      name: 'branch',
      component: () => import('../views/Branch/BranchView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/employee',
      name: 'employee',
      component: () => import('../views/Employee/EmployeeView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/product',
      name: 'product',
      component: () => import('../views/Product/ProductView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/salary',
      name: 'salary',
      component: () => import('../views/Salary/SalaryView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true },
      beforeEnter: [requireRole]
    },

    {
      path: '/sliphistory',
      name: 'sliphistory',
      component: () => import('../views/Salary/PaymentSlipHistory.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/invoice',
      name: 'invoice',
      component: () => import('../views/UtilityInvoice/InvoiceView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true },
      beforeEnter: [requireRole]
    },

    {
      path: '/login',
      name: 'logout',
      component: () => import('../views/LoginPage.vue'),
      meta: { layout: 'FullLayout', requireAuth: false }
    },
    
    {
      path: '/forgetpass',
      name: 'forgetpass',
      component: () => import('../views/ForgetPassView.vue'),
      meta: { layout: 'FullLayout', requireAuth: false }
    },

    {
      path: '/customer',
      name: 'customer',
      component: () => import('../views/Customer/CustomerView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },

    {
      path: '/material',
      name: 'material',
      component: () => import('../views/Material/MaterialView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },
    {
      path: '/promotion',
      name: 'promotion',
      component: () => import('../views/Promotion/PromotionView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },
    {
      path: '/checkstock',
      name: 'checkstock',
      component: () => import('../views/CheckStock/CheckstockView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },
    {
      path: '/importmaterial',
      name: 'importmaterial',
      component: () => import('../views/ImportMaterial/ImportMaterialView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true }
    },
    {
      path: '/branchstock',
      name: 'branchstock',
      component: () => import('../views/BranchStock/BranchStockView.vue'),
      meta: { layout: 'MainLayout', requireAuth: true  }
    }
  ]
})

const emp = ref<Employee | null >();
function isLogin() {
  
  const user = localStorage.getItem('user')
  if (user !== null) {
    emp.value = JSON.parse(user);
  }
  if (user) {
    console.log("อยู่ตรงนี้"+localStorage.getItem('user'))
    console.log("อยู่ตรงนี้231 ===="+emp.value?.roles[0].roleId)
    return true
  }
  return false
}
router.beforeEach((to, from) => {
  console.log(from)
  console.log(to)
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/login')
  }
})

export default router
